<?php

class Student
{
    public function __construct()
    {

    }

    public function calculateDues()
    {
        // do some calculation and check previous payment

    }

    public function getAttendance()
    {

    }

    public function getStudentInfo()
    {
        // get data from db

    }

    public function saveStudent()
    {
        // store in database
    }
}

// it should be 

class StudentDues
{
    public function __construct()
    {

    }

    public function calculate()
    {
        // do some calculation and check previous payment

    }
}

class Attendance
{
    public function getStudentAttendance()
    {

    }

    public function getEmployeeAttendance()
    {

    }

}

class StudentRepository
{
    public function getInfo()
    {
        // get data from db

    }

    public function save()
    {
        // store in database
    }
}
