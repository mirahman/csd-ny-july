const assert = require('assert')
const cal = require('./../calculator.js')
//const cal = new calculator();


it('adding two positive integer', () => {
    assert.equal(cal.add(1, 3), 4)
})

it('adding two positive fraction', () => {
    assert.equal(cal.add(1.7, 3.2), 4.9)
})

it('adding two negative number', () => {
    assert.equal(cal.add(-5, -6), -11)
})

it('adding two non number', () => {
    assert.deepStrictEqual(cal.add('A', 'B'), NaN)
})

it('adding two big integers', () => {
    assert.equal(cal.add(Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER), 2 * Number.MAX_SAFE_INTEGER)
})

it('subtracting two positive integer', () => {
    assert.equal(cal.subtract(10, 3), 7)
})

it('multiply 2 X 3', () => {
    assert.equal(cal.multiply(2, 3), 6)
})

it('dividing by 0', () => {
    assert.deepStrictEqual(cal.division(10, 0), Infinity)
})
