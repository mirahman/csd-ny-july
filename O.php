<?php

class Book 
{
    public $title;
    public $author;

    public function __construct($title, $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }
}

class BookAdapter extends Book {

    public function getTitleAndAuthor()
    {
        return $this->getAuthor()." & ".$this->getTitle();
    }
}

$adapter = new BookAdapter("PHP 7 Data structures and algorithms", "Mizanur Rahman");
echo $adapter->getTitleAndAuthor();