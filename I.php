<?php

interface RefactoringTopic {
    public function teachRefactoring();
    public function teachDesignPattern();
}

interface DevopsTopic {
    public function teachCICD();
    public function teachDevOps();
}

interface TDDTopic {
    public function teachTDD();
    public function teachBDD();
    public function teachATDD();
}

interface AgileEngineeringTopic extends TDDTopic, DevopsTopic, RefactoringTopic {

}

class CSD implements AgileEngineeringTopic
{
    public function teachRefactoring(){}
    public function teachDesignPattern(){}
    public function teachDevOps(){}
    public function teachTDD(){}
    public function teachBDD(){}
    public function teachATDD(){}
    public function teachCICD(){}
}

class TDDTraining implements TDDTopic
{
    public function teachTDD(){}
    public function teachBDD(){}
    public function teachATDD(){
        // blank implement
        // do not need it here
    }
}