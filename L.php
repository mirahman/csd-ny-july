<?php

class Book 
{
    public $title;
    public $author;

    public function __construct($title, $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    public function getTitle()
    {
        $this->logRequest("got a call");
        echo "I am from Book Class:: ".$this->title."\n";
    }

    public function logRequest($str)
    {
        echo "Request LOG:: ".$str."\n";
    }
}

class EBook extends Book {

    public function getTitle()
    {
        parent::getTitle();
        echo "I am from E-Book Class:: ".$this->title."\n";
    }
}

class AudioBook extends Book {

    public function getTitle()
    {
        parent::getTitle();
        echo "I am from Audio Book Class:: ".$this->title."\n";
    }
}

$adapter = new Book("PHP 7 Data structures and algorithms", "Mizanur Rahman");
echo $adapter->getTitle();

$adapter = new EBook("PHP 7 Data structures and algorithms", "Mizanur Rahman");
echo $adapter->getTitle();

$adapter = new AudioBook("PHP 7 Data structures and algorithms", "Mizanur Rahman");
echo $adapter->getTitle();