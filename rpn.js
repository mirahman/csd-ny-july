String.prototype.isNumeric = function () {
    return !isNaN(parseFloat(this)) && isFinite(this);
}

let solve = function solve(postfix) {
    var resultStack = [];
    postfix = postfix.split(" ");
    for (var i = 0; i < postfix.length; i++) {
        let char = postfix[i];
        if (char.isNumeric()) {
            resultStack.push(char);
        } else {
            var a = resultStack.pop();
            var b = resultStack.pop();
            if (char === "+") {
                resultStack.push(parseInt(a) + parseInt(b));
            } else if (char === "-") {
                resultStack.push(parseInt(b) - parseInt(a));
            } else if (char === "*") {
                resultStack.push(parseInt(a) * parseInt(b));
            } else if (char === "/") {
                resultStack.push(parseInt(b) / parseInt(a));
            } else if (char === "^") {
                resultStack.push(Math.pow(parseInt(b), parseInt(a)));
            }
        }
    }
    return resultStack.pop();
}

module.exports = {
    solve,
}