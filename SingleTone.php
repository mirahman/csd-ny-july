<?php
class SingleTone
{
    public static $instance;
    
    private function __construct()
    {

    }

    public static function getInstance(): SingleTone
    {
        if(is_null(self::$instance)) {
            self::$instance = new SingleTone();
        }
        return self::$instance;
    }

}

$var1 = SingleTone::getInstance();
$var2 = SingleTone::getInstance();

var_dump($var1, $var2);