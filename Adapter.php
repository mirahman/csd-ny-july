<?php

class Book 
{
    public $title;
    public $author;

    public function __construct($title, $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }
}

class BookAdapter {
    public $book;

    public function __construct($book)
    {
        $this->book = $book;
    }

    public function getTitleAndAuthor()
    {
        return $this->book->getAuthor()." & ".$this->book->getTitle();
    }
}


$phpBook = new Book("PHP 7 Data structures and algorithms", "Mizanur Rahman");
$adapter = new BookAdapter($phpBook);
echo $adapter->getTitleAndAuthor();