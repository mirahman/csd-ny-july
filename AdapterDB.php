<?php
interface DataInterface
{
    public function getData();
}

class Student implements DataInterface
{
    public function getData()
    {
        $data = [
            ["a", 1],
            ["b", 2],
            ["c", 3],
            ["d", 4]
        ];
        return $data;
    }
}

class Course implements DataInterface
{
    public function getData()
    {
        $data = [
            ["CSM", "ABCD EFG", 1000],
            ["CSPO", "XYX",  2000],
            ["CSD", "Mizan",  1200]
        ];
        return $data;
    }
}


class Teacher implements DataInterface
{
    public function getData()
    {
        $data = [
            ["CSM", "ABCD EFG", 1000],
            ["CSPO", "XYX",  2000],
            ["CSD", "Mizan",  1200]
        ];
        return $data;
    }
}

class CSVAdapter {
    public $dataObj;

    public function __construct(DataInterface $dataObj)
    {
        $this->dataObj = $dataObj;
    }

    public function getCSV()
    {
        $data = $this->dataObj->getData();
        $csvString = "";
        foreach($data as $row) {
            $csvString .= implode(";",$row)."\n";
        }
        return $csvString;
    }
}


$mizan = new Student();
$course = new Course();
$teacher = new Teacher();

$myAdapter = new CSVAdapter($mizan);
echo $myAdapter->getCSV();
$myAdapterCourse = new CSVAdapter($course);
echo $myAdapterCourse->getCSV();

$myAdapterTeacher = new CSVAdapter($teacher);
echo $myAdapterTeacher->getCSV();


// Student      Teacher
// composition over inheritances
                                                                                                                
class StudentTeacher
{
    public $student;
    public $teacher;

    public function __constuct($student, $teacher)
    {
        $this->student = $student;
        $this->teacher = $teacher;
    }

    public function getStudentName()
    {
        return $this->student->getName();
    }

    public function getTeacherName()
    {
        return $this->teacher->getName();
    }
}
