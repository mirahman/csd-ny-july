/*
const { Before, Given, When, Then } = require('cucumber')
const Calculator = require('../../lib/calculator');
const assert = require('assert')

let calculator;
/*
Given('the numbers {int} and {int}', function (x, y) {
    // Write code here that turns the phrase above into concrete actions
    calculator = new Calculator(x, y);
});

When('they are added together', function () {
    // Write code here that turns the phrase above into concrete actions
    calculator.add();
});

Then('should the result be {int}', function (expected) {
    // Write code here that turns the phrase above into concrete actions
    assert.equal(calculator.getResult(), expected)
});
*/
/*
Given('the numbers {int} and {int}', function (int, int2) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('they are added together', function () {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('should the result be {int}', function (int) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});


Given('the numbers {int} and {int}', function (x, y) {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    calculator = new Calculator(x, y);
});

When('they are added together', function () {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    calculator.add();
});

Then('should the result be {int}', function (result) {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    assert.equal(calculator.getResult(), result);
});
*/

const { Given, When, Then, Before } = require('cucumber');
const Calculator = require('../../calculatorclass');
const assert = require('assert')

let calculator;


Given('the numbers {int} and {int}', function (x, y) {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    calculator = new Calculator(x, y);
});

Given('the numbers {float} and {float}', function (x, y) {
    // Write code here that turns the phrase above into concrete actions
    calculator = new Calculator(x, y);
});

When('they are added together', function () {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    calculator.add();
});

Then('should the result be {int}', function (result) {
    // Write code here that turns the phrase above into concrete actions
    //return 'pending';
    assert.equal(calculator.getResult(), result)
});

Then('should the result be {float}', function (result) {
    // Write code here that turns the phrase above into concrete actions
    assert.equal(Number((calculator.getResult()).toFixed(1)), result)
});
