Feature: big Addition

  Addition is great as a verification exercise to get the Cucumber-js infrastructure up and running

  Scenario Outline: Add two number
    Given the numbers <a> and <b>
    When they are added together 
    Then should the result be <c>

  Examples:
    | a | b | c |
    | 1 | 0 | 1      |
    | 1 | 1 | 2      |
    | 2 | 2 | 4      |
    | 1000 | 2000 | 3000      |
    | 10.7 | 2.2 | 12.9      |