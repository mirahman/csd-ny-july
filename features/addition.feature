Feature: Addition

  Addition testing with BDD

  Scenario: Add two number
    Given the numbers 1 and 4
    When they are added together 
    Then should the result be 5

  Scenario: Add two negative number
    Given the numbers -1 and -3
    When they are added together 
    Then should the result be -4

  Scenario: Add one negative and one positive number
    Given the numbers -4 and 10
    When they are added together 
    Then should the result be 6

  Scenario: Add two fractional number
    Given the numbers 4.5 and 10.7
    When they are added together 
    Then should the result be 15.2