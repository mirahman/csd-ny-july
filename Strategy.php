<?php

interface notification
{
    public function send($obj);
}

class SMSStrategy implements notification
{
    public function send($notify)
    {
        echo "Sending SMS: ".$notify->text;
    }
}

class EmailStrategy implements notification
{
    public function send($notify)
    {
        echo "Sending Email: ".$notify->text;
    }
}

class PushNotificationStrategy implements notification
{
    public function send($notify)
    {
        echo "Sending Push Notification: ".$notify->text;
    }
}

class Notify 
{
    public $text;

    public function __construct($text)
    {
        $this->text = $text;
    }
}

$billinNotification = new Notify("Your billing data has been updated");
$jobNotification = new Notify("We have 10 new jobs for you at linkedin");
try {

    if(isWeekend()) {
    $notify = new PushNotificationStrategy();
    $notify->send($billinNotification);
} else {
    $notify = new SMSStrategy();
    $notify->send($billinNotification);
}

} catch (Exception $e) {
    $notify = new EmailStrategy();
    $notify->send($jobNotification);
}





function isWeekend() {
    return true; // todo: write some logic based on date
}
