const add = (x, y) => +x + +y
const subtract = (x, y) => +x - +y
const multiply = (x, y) => +x * +y
const division = (x, y) => +x / +y

module.exports = {
    add,
    subtract,
    multiply,
    division,
}